package com.example.aop_part3_chapter02

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import androidx.viewpager2.widget.ViewPager2
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import org.json.JSONArray
import org.json.JSONObject
import kotlin.math.absoluteValue

class MainActivity : AppCompatActivity() {

    private val viewPager: ViewPager2 by lazy {
        findViewById(R.id.viewPager)
    }

    private val progressBar: ProgressBar by lazy {
        findViewById(R.id.progressBar)
    }

    private val showPage: TextView by lazy {
        findViewById(R.id.showPage)
    }

    private val goHome: ImageButton by lazy {
        findViewById(R.id.goHome)
    }

    private val goForward: ImageButton by lazy {
        findViewById(R.id.goForward)
    }

    private val goBack: ImageButton by lazy {
        findViewById(R.id.goBack)
    }

    private var firstPage: Int = 0
    private var quotesSize: Int = 0
    private var nowPage: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        initData()
        initButton()
    }

    @SuppressLint("SetTextI18n")
    private fun initButton() {
        goHome.setOnClickListener {
            firstPage?.let { it1 -> viewPager.setCurrentItem(it1, false) }
            nowPage = 1
            showPage.text = "${nowPage} / ${quotesSize}"
        }

        goBack.setOnClickListener {
            viewPager.setCurrentItem(viewPager.currentItem-1, true)
            if (nowPage - 1 <= 0) {
                nowPage = quotesSize + ((nowPage - 1) % quotesSize)
            } else {
                nowPage = (nowPage - 1) % quotesSize
            }
            showPage.text = "${nowPage} / ${quotesSize}"
        }

        goForward.setOnClickListener {
            viewPager.setCurrentItem(viewPager.currentItem+1, true)
            nowPage = (nowPage + 1) % quotesSize
            if (nowPage == 0) nowPage = 5
            showPage.text = "${nowPage} / ${quotesSize}"
        }
    }

    private fun initViews() {
        viewPager.setPageTransformer{ page, position ->
            when {
                position.absoluteValue >= 1F -> {
                    page.alpha = 0F
                }
                position == 0F -> {
                    page.alpha = 1F
                }
                else -> {
                    page.alpha = 1F - 2 * position.absoluteValue
                }
            }

        }
    }

    private fun initData() {
        val remoteConfig = Firebase.remoteConfig
        remoteConfig.setConfigSettingsAsync(
            remoteConfigSettings {
                minimumFetchIntervalInSeconds = 0
            }
        )
        remoteConfig.fetchAndActivate().addOnCompleteListener{
            progressBar.visibility = View.GONE
            if (it.isSuccessful) {
                val quotes = parseQuotesJson(remoteConfig.getString("quotes"))
                val isNameRevealed = remoteConfig.getBoolean("is_name_revealed")

                goForward.visibility = View.VISIBLE
                goBack.visibility = View.VISIBLE
                goHome.visibility = View.VISIBLE

                displayQuotesPager(quotes, isNameRevealed)

            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun displayQuotesPager(quotes: List<Quote>, isNameRevealed: Boolean) {
        val adapter = QuotesPagerAdapter(
            quotes = quotes,
            isNameRevealed = isNameRevealed
        )
        quotesSize = quotes.size
        firstPage = ((adapter.itemCount / 2) - (adapter.itemCount % quotesSize)) - 1
        viewPager.adapter = adapter
        viewPager.setCurrentItem(firstPage, false)
        showPage.text = "1 / ${quotesSize}"
    }

    private fun parseQuotesJson(json: String): List<Quote> {
        val jsonArray = JSONArray(json)
        var jsonList = emptyList<JSONObject>()
        for(index in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(index)
            jsonObject?.let {
                jsonList = jsonList + it
            }
        }

        return jsonList.map {
            Quote(
                quote = it.getString("quote"),
                name = it.getString("name")
            )
        }
    }
}